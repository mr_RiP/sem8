﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    partial class MainForm
    {
        private Sensors sensors = null;
        private bool buttonPressed = false;
        private bool mouseInside = false;

        private void InitilaizeCanvas()
        {
            sensors = new Sensors();
            picS.Image = sensors.Bitmap;
        }

        private void picS_MouseDown(object sender, MouseEventArgs e)
        {
            buttonPressed = true;
            sensors.EditBitmap(e.X, e.Y, e.Button == MouseButtons.Left);
            picS.Refresh();
        }

        private void picS_MouseMove(object sender, MouseEventArgs e)
        {
            if (buttonPressed && mouseInside)
            {
                sensors.EditBitmap(e.X, e.Y, e.Button == MouseButtons.Left);
                picS.Refresh();
            }
        }

        private void picS_MouseUp(object sender, MouseEventArgs e)
        {
            buttonPressed = false;
        }

        private void picS_MouseEnter(object sender, EventArgs e)
        {
            mouseInside = true;
        }

        private void picS_MouseLeave(object sender, EventArgs e)
        {
            mouseInside = false;
        }

        private void ShowExample()
        {
            List<int> selectedResults = GetSelectedResults();
            if (selectedResults.Count == 0)
                ShowExampleMessage("Отображение невозможно: не выбран ни один результат.");
            else if (selectedResults.Count > 1)
                ShowExampleMessage("Отображение невозможно: выбрано более одного результата.");
            else
            {
                sensors.LoadExample(examples[selectedResults.First()]);
                picS.Refresh();
                ClearResults();
            }
        }

        private void ShowExampleMessage(string message)
        {
            MessageBox.Show(message, "Отображение", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
