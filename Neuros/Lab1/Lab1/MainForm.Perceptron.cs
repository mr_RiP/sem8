﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    partial class MainForm
    {
        private const int resultsCount = 6;
        private Perceptron perceptron = null;
        private List<bool[,]> examples = null;

        private void InitializePerceptron()
        {
            perceptron = new Perceptron(sensors.MatrixCount(), resultsCount);
            examples = GetExamples();
        }

        private List<bool[,]> GetExamples()
        {
            string currentDirectoryPath = Directory.GetCurrentDirectory();
            var bitmapList = new List<Bitmap>();
            bitmapList.Add(new Bitmap(Path.Combine(currentDirectoryPath, "Examples\\A.bmp"), true));
            bitmapList.Add(new Bitmap(Path.Combine(currentDirectoryPath, "Examples\\B.bmp"), true));
            bitmapList.Add(new Bitmap(Path.Combine(currentDirectoryPath, "Examples\\C.bmp"), true));
            bitmapList.Add(new Bitmap(Path.Combine(currentDirectoryPath, "Examples\\X.bmp"), true));
            bitmapList.Add(new Bitmap(Path.Combine(currentDirectoryPath, "Examples\\Y.bmp"), true));
            bitmapList.Add(new Bitmap(Path.Combine(currentDirectoryPath, "Examples\\Z.bmp"), true));

            List<bool[,]> resultList = bitmapList
                .Select(bitmap => Sensors.BitmapToMatrix(bitmap))
                .ToList();

            foreach (var bitmap in bitmapList)
                bitmap.Dispose();

            return resultList;
        }

        private void Teach()
        {
            List<int> results = GetSelectedResults();
            if (results.Count == 0)
            {
                List<int> errorList = perceptron.Teach(examples);
                ShowTeachingMessage("Перцептрон обучен за " + errorList.Count + " итераций.\n" +
                    "Количество ошибок в последней итерации: " + errorList.Last() + ".\n" + 
                    "Количество мертвых нейронов: " + perceptron.GetDeadNeurons() + ".");
            }
            else if (results.Count > 1)
            {
                ShowTeachingMessage("Обучение невозможно: Выбрано более одного результата.\n", MessageBoxIcon.Error);
            }
            else
            {
                int errors = perceptron.TeachSpecific(sensors.Matrix, results.First());
                ShowTeachingMessage("Обучение на частном примере завершено.\n" +
                    "Количество ошибок: " + errors + ".\n" +
                    "Количество мертвых нейронов: " + perceptron.GetDeadNeurons() + ".");
            }
        }

        private void ShowTeachingMessage(string message, MessageBoxIcon icon = MessageBoxIcon.Information)
        {
            MessageBox.Show(message, "Обучение", MessageBoxButtons.OK, icon);
        }

        private List<int> GetSelectedResults()
        {
            var results = new List<int>();
            if (cbA.Checked)
                results.Add(0);
            if (cbB.Checked)
                results.Add(1);
            if (cbC.Checked)
                results.Add(2);
            if (cbX.Checked)
                results.Add(3);
            if (cbY.Checked)
                results.Add(4);
            if (cbZ.Checked)
                results.Add(5);
            return results;
        }

        private void DisplayResults(bool[] results)
        {
            cbA.Checked = results[0];
            cbB.Checked = results[1];
            cbC.Checked = results[2];
            cbX.Checked = results[3];
            cbY.Checked = results[4];
            cbZ.Checked = results[5];
        }

        private void GetResults()
        {
            bool[] results = perceptron.GetResult(sensors.Matrix);
            DisplayResults(results);
        }

        private void ClearResults()
        {
            cbA.Checked = false;
            cbB.Checked = false;
            cbC.Checked = false;
            cbX.Checked = false;
            cbY.Checked = false;
            cbZ.Checked = false;
        }
    }
}
