﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitilaizeCanvas();
            InitializePerceptron();
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            sensors.Flush();
            ClearResults();
            picS.Refresh();
        }

        private void btnTeach_Click(object sender, EventArgs e)
        {
            Teach();
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            GetResults();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ShowExample();
        }
    }
}
