﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Perceptron
    {
        public int CountS { get; private set; }
        public int CountA { get; private set; }
        public int CountR { get; private set; }

        public bool[,] WeightsSA { get; private set; }
        public double[,] WeightsAR { get; private set; }
        public double[] Bias { get; private set; }

        public double LearningSpeedModifier { get; set; } = 0.001;
        public int AMultiplier { get; set; } = 10;

        public Perceptron(int sCount, int rCount)
        {
            CountS = sCount;
            CountR = rCount;
            CountA = sCount * AMultiplier;

            InitializeWeightsSA();
            InitializeWeightsAR();
            InitializeBias();
        }

        private void InitializeBias()
        {
            var random = new Random();
            Bias = new double[CountR];
            for (int i = 0; i < CountR; ++i)
                Bias[i] = random.NextDouble();
        }

        private void InitializeWeightsAR()
        {
            var random = new Random();
            WeightsAR = new double[CountA, CountR];
            for (int i = 0; i < CountA; ++i)
                for (int j = 0; j < CountR; ++j)
                    WeightsAR[i, j] = 0.0;
        }

        private void InitializeWeightsSA()
        {
            var random = new Random();
            WeightsSA = new bool[CountS, CountA];
            for (int i = 0; i < CountS; ++i)
                for (int j = 0; j < CountA; ++j)
                    WeightsSA[i, j] = random.NextDouble() < 0.2;
        }

        public List<int> Teach(List<bool[,]> examples, int iterationsCount = 100)
        {
            if (examples.Count < CountR)
                throw new ArgumentOutOfRangeException("examples");
            if (iterationsCount <= 0)
                throw new ArgumentOutOfRangeException("iterationsCount");

            var errorList = new List<int>(iterationsCount);
            int errorCount = -1;
            for (int i = 0; i < iterationsCount && errorCount != 0; ++i)
            {
                errorCount = 0;
                for (int j = 0; j < CountR; ++j)
                    errorCount += TeachSpecific(examples[j], j);
                errorList.Add(errorCount);
            }

            return errorList;
        }

        public int TeachSpecific(bool[,] example, int resultIndex)
        {
            if (resultIndex < 0 || resultIndex >= CountR)
                throw new ArgumentOutOfRangeException("resultIndex");

            bool[] s = example.Cast<bool>().ToArray();
            if (s.Length != CountS)
                throw new ArgumentOutOfRangeException("example");

            int[] a = GetA(s);
            bool[] r = GetR(a);
            bool[] errors = DetectErrors(a, r, resultIndex);
                AjustWeights(errors, a, resultIndex);
            int errorsRemains = DetectErrors(a, r, resultIndex).Where(e => e).Count();
            return errorsRemains;
        }

        private bool[] DetectErrors(int[] a, bool[] r, int resultIndex)
        {
            bool[] errors = new bool[CountR];
            for (int i = 0; i < CountR; ++i)
                errors[i] = i == resultIndex && !r[i] || i != resultIndex && r[i];
            return errors;
        }

        private void AjustWeights(bool[] errors, int[] a, int resultIndex)
        {
            for (int i = 0; i < CountR; ++i)
                if (errors[i])
                {
                    if (i == resultIndex)
                    {
                        for (int j = 0; j < CountA; ++j)
                            WeightsAR[j, i] += LearningSpeedModifier * a[j];
                        Bias[i] -= LearningSpeedModifier;
                    }
                    else
                    {
                        for (int j = 0; j < CountA; ++j)
                            WeightsAR[j, i] -= LearningSpeedModifier * a[j];
                        Bias[i] += LearningSpeedModifier;
                    }
                }
        }

        private bool[] GetR(int[] a)
        {
            var r = new bool[CountR];
            var rSum = new double[CountR];
            for (int i = 0; i < CountR; ++i)
            {
                for (int j = 0; j < CountA; ++j)
                    rSum[i] += WeightsAR[j, i] * a[j];

                r[i] = rSum[i] > Bias[i];
            }

            return r;
        }

        private int[] GetA(bool[] s)
        {
            var a = new int[CountA];
            for (int i = 0; i < CountA; ++i)
                for (int j = 0; j < CountS; ++j)
                    if (s[j] && WeightsSA[j, i])
                        a[i]++;

            return a;
        }

        public int GetDeadNeurons()
        {
            int sum = 0;
            for (int i = 0; i < CountA; ++i)
            {
                int innerSum = 0;
                for (int j = 0; j < CountR; ++j)
                    if (WeightsAR[i, j] == 0)
                        innerSum++;

                if (innerSum == CountR)
                    sum++;
            }
            return sum;
        }

        public bool[] GetResult(bool[,] scanData)
        {
            var s = scanData.Cast<bool>().ToArray();
            if (s.Length != CountS)
                throw new ArgumentOutOfRangeException();

            int[] a = GetA(s);
            return GetR(a);
        }
    }
}
