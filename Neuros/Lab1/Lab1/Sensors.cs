﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Sensors
    {
        const int bitmapN = 200;
        const int matrixN = 20;
        const int pixelSize = bitmapN / matrixN;

        public Sensors()
        {
            InitializeBitmap();
            InitializeMatrix();
        }

        public void Flush()
        {
            FlushMatrix();
            FlushBitmap();
        }

        private void FlushBitmap()
        {
            for (int i = 0; i < bitmapN; ++i)
                for (int j = 0; j < bitmapN; ++j)
                    Bitmap.SetPixel(j, i, Color.White);
        }

        private void FlushMatrix()
        {
            for (int i = 0; i < matrixN; ++i)
                for (int j = 0; j < matrixN; ++j)
                    Matrix[i, j] = false;
        }

        private void InitializeMatrix()
        {
            Matrix = new bool[matrixN, matrixN];
            FlushMatrix();
        }

        private void InitializeBitmap()
        {
            Bitmap = new Bitmap(bitmapN, bitmapN);
            FlushBitmap();
        }

        public Bitmap Bitmap { get; private set; }

        public bool[,] Matrix { get; private set; }

        public int MatrixCount()
        {
            return matrixN * matrixN;
        }

        public int BitmapCount()
        {
            return bitmapN * bitmapN;
        }

        public void EditBitmap(int x, int y, bool value)
        {
            if (x >= 0 && x < bitmapN && y >= 0 && y < bitmapN)
            {
                x /= pixelSize;
                y /= pixelSize;

                EditMatrix(y, x, value);
            }
        }

        public void EditMatrix(int i, int j, bool value)
        {
            if (i >= 0 && i < matrixN && j >= 0 && j < matrixN)
            {
                Matrix[i, j] = value;
                DrawPixels(j, i, value ? Color.Black : Color.White);
            }
        }

        public void LoadExample(bool[,] exampleMatrix)
        {
            if (exampleMatrix.Rank != 2 || exampleMatrix.GetLength(0) != matrixN || exampleMatrix.GetLength(1) != matrixN)
                throw new ArgumentOutOfRangeException();

            for (int i = 0; i < matrixN; ++i)
                for (int j = 0; j < matrixN; ++j)
                    EditMatrix(i, j, exampleMatrix[i, j]);
        }

        private void DrawPixels(int x, int y, Color color)
        {
            int xStart = x * pixelSize;
            int xFinish = xStart + pixelSize;

            int yStart = y * pixelSize;
            int yFinish = yStart + pixelSize;

            for (x = xStart; x < xFinish; ++x)
                for (y = yStart; y < yFinish; ++y)
                    Bitmap.SetPixel(x, y, color);
        }

        public static bool[,] BitmapToMatrix(Bitmap bitmap)
        {
            var matrix = new bool[matrixN, matrixN];
            int argbBlack = Color.Black.ToArgb();
            using (var scaledBitmap = new Bitmap(bitmap, new Size(matrixN, matrixN)))
            {
                for (int i = 0; i < matrixN; ++i)
                    for (int j = 0; j < matrixN; ++j)
                        matrix[i, j] = scaledBitmap.GetPixel(j, i).ToArgb() == argbBlack;
            }
            return matrix;
        }
    }
}
