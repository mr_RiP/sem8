﻿namespace Lab1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picS = new System.Windows.Forms.PictureBox();
            this.btnTeach = new System.Windows.Forms.Button();
            this.btnClean = new System.Windows.Forms.Button();
            this.btnScan = new System.Windows.Forms.Button();
            this.cbA = new System.Windows.Forms.CheckBox();
            this.cbB = new System.Windows.Forms.CheckBox();
            this.cbC = new System.Windows.Forms.CheckBox();
            this.cbX = new System.Windows.Forms.CheckBox();
            this.cbZ = new System.Windows.Forms.CheckBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.cbY = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.picS)).BeginInit();
            this.SuspendLayout();
            // 
            // picS
            // 
            this.picS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picS.Location = new System.Drawing.Point(12, 12);
            this.picS.Name = "picS";
            this.picS.Size = new System.Drawing.Size(200, 200);
            this.picS.TabIndex = 0;
            this.picS.TabStop = false;
            this.picS.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picS_MouseDown);
            this.picS.MouseEnter += new System.EventHandler(this.picS_MouseEnter);
            this.picS.MouseLeave += new System.EventHandler(this.picS_MouseLeave);
            this.picS.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picS_MouseMove);
            this.picS.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picS_MouseUp);
            // 
            // btnTeach
            // 
            this.btnTeach.Location = new System.Drawing.Point(218, 160);
            this.btnTeach.Name = "btnTeach";
            this.btnTeach.Size = new System.Drawing.Size(75, 23);
            this.btnTeach.TabIndex = 1;
            this.btnTeach.Text = "Обучить";
            this.btnTeach.UseVisualStyleBackColor = true;
            this.btnTeach.Click += new System.EventHandler(this.btnTeach_Click);
            // 
            // btnClean
            // 
            this.btnClean.Location = new System.Drawing.Point(218, 131);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(75, 23);
            this.btnClean.TabIndex = 2;
            this.btnClean.Text = "Очистить";
            this.btnClean.UseVisualStyleBackColor = true;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(218, 189);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(75, 23);
            this.btnScan.TabIndex = 3;
            this.btnScan.Text = "Результат";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // cbA
            // 
            this.cbA.AutoSize = true;
            this.cbA.Location = new System.Drawing.Point(218, 21);
            this.cbA.Name = "cbA";
            this.cbA.Size = new System.Drawing.Size(33, 17);
            this.cbA.TabIndex = 4;
            this.cbA.Text = "A";
            this.cbA.UseVisualStyleBackColor = true;
            // 
            // cbB
            // 
            this.cbB.AutoSize = true;
            this.cbB.Location = new System.Drawing.Point(218, 44);
            this.cbB.Name = "cbB";
            this.cbB.Size = new System.Drawing.Size(33, 17);
            this.cbB.TabIndex = 5;
            this.cbB.Text = "B";
            this.cbB.UseVisualStyleBackColor = true;
            // 
            // cbC
            // 
            this.cbC.AutoSize = true;
            this.cbC.Location = new System.Drawing.Point(218, 67);
            this.cbC.Name = "cbC";
            this.cbC.Size = new System.Drawing.Size(33, 17);
            this.cbC.TabIndex = 6;
            this.cbC.Text = "C";
            this.cbC.UseVisualStyleBackColor = true;
            // 
            // cbX
            // 
            this.cbX.AutoSize = true;
            this.cbX.Location = new System.Drawing.Point(255, 21);
            this.cbX.Name = "cbX";
            this.cbX.Size = new System.Drawing.Size(33, 17);
            this.cbX.TabIndex = 7;
            this.cbX.Text = "X";
            this.cbX.UseVisualStyleBackColor = true;
            // 
            // cbZ
            // 
            this.cbZ.AutoSize = true;
            this.cbZ.Location = new System.Drawing.Point(255, 67);
            this.cbZ.Name = "cbZ";
            this.cbZ.Size = new System.Drawing.Size(33, 17);
            this.cbZ.TabIndex = 8;
            this.cbZ.Text = "Z";
            this.cbZ.UseVisualStyleBackColor = true;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(218, 102);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 23);
            this.btnShow.TabIndex = 9;
            this.btnShow.Text = "Отобразить";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cbY
            // 
            this.cbY.AutoSize = true;
            this.cbY.Location = new System.Drawing.Point(255, 44);
            this.cbY.Name = "cbY";
            this.cbY.Size = new System.Drawing.Size(33, 17);
            this.cbY.TabIndex = 10;
            this.cbY.Text = "Y";
            this.cbY.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AcceptButton = this.btnScan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 221);
            this.Controls.Add(this.cbY);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.cbZ);
            this.Controls.Add(this.cbX);
            this.Controls.Add(this.cbC);
            this.Controls.Add(this.cbB);
            this.Controls.Add(this.cbA);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.btnClean);
            this.Controls.Add(this.btnTeach);
            this.Controls.Add(this.picS);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "Перцептрон Розенблатта";
            ((System.ComponentModel.ISupportInitialize)(this.picS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picS;
        private System.Windows.Forms.Button btnTeach;
        private System.Windows.Forms.Button btnClean;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.CheckBox cbA;
        private System.Windows.Forms.CheckBox cbB;
        private System.Windows.Forms.CheckBox cbC;
        private System.Windows.Forms.CheckBox cbX;
        private System.Windows.Forms.CheckBox cbZ;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.CheckBox cbY;
    }
}

