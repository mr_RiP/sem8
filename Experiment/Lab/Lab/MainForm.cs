﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab
{
    public partial class MainForm : Form
    {
        string savedText = null;

        public MainForm()
        {
            InitializeComponent();
            InitializeParameterGroups();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            try
            {
                Calculate();
            }
            catch (Exception exception)
            {
                ErrorMessage(exception.Message);
            }
        }

        private void SaveText(object sender, EventArgs e)
        {
            var txtSender = sender as TextBox;
            savedText = txtSender.Text;
        }

        private void ResetText(TextBox textBox)
        {
            textBox.Text = savedText;
        }

        private void PositiveDoubleCheckFix(object sender, EventArgs e)
        {
            var txtSender = sender as TextBox;
            if (double.TryParse(txtSender.Text, out double result) && result > 0.0)
            {
                if (UniformParameter(txtSender)) 
                    UniformCheckFix(txtSender);
                else
                    RayleighCheckFix(txtSender);
            }
            else
            {
                ErrorMessage("Требуемое значение должно быть действительным числом больше нуля");
                ResetText(txtSender);
            }
        }

        private void PositiveIntCheckFix(object sender, EventArgs e)
        {
            var txtSender = sender as TextBox;
            if ((int.TryParse(txtSender.Text, out int result) && result > 0))
            {
                count = result;
            }
            else
            {
                ErrorMessage("Требуемое значение должно быть целым числом больше нуля");
                ResetText(txtSender);
            }
        }

        private bool CalculateWholeTimeMode()
        {
            return rbWholeTime.Checked;
        }
    }
}
