﻿using System;

namespace Lab
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtInAvgMean = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtInAvgB = new System.Windows.Forms.TextBox();
            this.txtInAvgA = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtInMaxMean = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtInMaxB = new System.Windows.Forms.TextBox();
            this.txtInMaxA = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtInMinMean = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtInMinB = new System.Windows.Forms.TextBox();
            this.txtInMinA = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtOutAvgMean = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtOutAvgSigma = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtOutMaxMean = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtOutMaxSigma = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOutMinMean = new System.Windows.Forms.TextBox();
            this.txtOutMinSigma = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rbPerTransactTime = new System.Windows.Forms.RadioButton();
            this.label30 = new System.Windows.Forms.Label();
            this.rbWholeTime = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCount = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTimeExpected1 = new System.Windows.Forms.TextBox();
            this.txtTimeExpected2 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtTimeHappened = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtB3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtYMinMin = new System.Windows.Forms.TextBox();
            this.txtB2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtYMaxMin = new System.Windows.Forms.TextBox();
            this.txtB1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtYMinMax = new System.Windows.Forms.TextBox();
            this.txtB0 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtYMaxMax = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox10);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(456, 140);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поступление заявок";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Controls.Add(this.txtInAvgMean);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.txtInAvgB);
            this.groupBox10.Controls.Add(this.txtInAvgA);
            this.groupBox10.Location = new System.Drawing.Point(304, 32);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(143, 101);
            this.groupBox10.TabIndex = 6;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Среднее";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(8, 74);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(19, 13);
            this.label24.TabIndex = 8;
            this.label24.Text = "M:";
            // 
            // txtInAvgMean
            // 
            this.txtInAvgMean.Location = new System.Drawing.Point(37, 71);
            this.txtInAvgMean.Name = "txtInAvgMean";
            this.txtInAvgMean.ReadOnly = true;
            this.txtInAvgMean.Size = new System.Drawing.Size(100, 20);
            this.txtInAvgMean.TabIndex = 7;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(8, 48);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "B:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(8, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(17, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "A:";
            // 
            // txtInAvgTo
            // 
            this.txtInAvgB.Location = new System.Drawing.Point(37, 45);
            this.txtInAvgB.Name = "txtInAvgTo";
            this.txtInAvgB.Size = new System.Drawing.Size(100, 20);
            this.txtInAvgB.TabIndex = 1;
            this.txtInAvgB.Text = "10";
            this.txtInAvgB.Enter += new System.EventHandler(this.SaveText);
            this.txtInAvgB.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // txtInAvgFrom
            // 
            this.txtInAvgA.Location = new System.Drawing.Point(37, 19);
            this.txtInAvgA.Name = "txtInAvgFrom";
            this.txtInAvgA.Size = new System.Drawing.Size(100, 20);
            this.txtInAvgA.TabIndex = 0;
            this.txtInAvgA.Text = "5";
            this.txtInAvgA.Enter += new System.EventHandler(this.SaveText);
            this.txtInAvgA.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.txtInMaxMean);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.txtInMaxB);
            this.groupBox5.Controls.Add(this.txtInMaxA);
            this.groupBox5.Location = new System.Drawing.Point(155, 32);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(143, 101);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Максимальное";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 74);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "M:";
            // 
            // txtInMaxMean
            // 
            this.txtInMaxMean.Location = new System.Drawing.Point(37, 71);
            this.txtInMaxMean.Name = "txtInMaxMean";
            this.txtInMaxMean.ReadOnly = true;
            this.txtInMaxMean.Size = new System.Drawing.Size(100, 20);
            this.txtInMaxMean.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "B:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "A:";
            // 
            // txtInMaxTo
            // 
            this.txtInMaxB.Location = new System.Drawing.Point(37, 45);
            this.txtInMaxB.Name = "txtInMaxTo";
            this.txtInMaxB.Size = new System.Drawing.Size(100, 20);
            this.txtInMaxB.TabIndex = 1;
            this.txtInMaxB.Text = "15";
            this.txtInMaxB.Enter += new System.EventHandler(this.SaveText);
            this.txtInMaxB.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // txtInMaxFrom
            // 
            this.txtInMaxA.Location = new System.Drawing.Point(37, 19);
            this.txtInMaxA.Name = "txtInMaxFrom";
            this.txtInMaxA.Size = new System.Drawing.Size(100, 20);
            this.txtInMaxA.TabIndex = 0;
            this.txtInMaxA.Text = "5";
            this.txtInMaxA.Enter += new System.EventHandler(this.SaveText);
            this.txtInMaxA.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.txtInMinMean);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txtInMinB);
            this.groupBox4.Controls.Add(this.txtInMinA);
            this.groupBox4.Location = new System.Drawing.Point(6, 32);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(143, 100);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Минимальное";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 74);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "M:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "B:";
            // 
            // txtInMinMean
            // 
            this.txtInMinMean.Location = new System.Drawing.Point(37, 71);
            this.txtInMinMean.Name = "txtInMinMean";
            this.txtInMinMean.ReadOnly = true;
            this.txtInMinMean.Size = new System.Drawing.Size(100, 20);
            this.txtInMinMean.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "A:";
            // 
            // txtInMinTo
            // 
            this.txtInMinB.Location = new System.Drawing.Point(37, 45);
            this.txtInMinB.Name = "txtInMinTo";
            this.txtInMinB.Size = new System.Drawing.Size(100, 20);
            this.txtInMinB.TabIndex = 1;
            this.txtInMinB.Text = "10";
            this.txtInMinB.Enter += new System.EventHandler(this.SaveText);
            this.txtInMinB.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // txtInMinFrom
            // 
            this.txtInMinA.Location = new System.Drawing.Point(37, 19);
            this.txtInMinA.Name = "txtInMinFrom";
            this.txtInMinA.Size = new System.Drawing.Size(100, 20);
            this.txtInMinA.TabIndex = 0;
            this.txtInMinA.Text = "1";
            this.txtInMinA.Enter += new System.EventHandler(this.SaveText);
            this.txtInMinA.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Равномерный закон";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(12, 158);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(456, 112);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Обработка заявок";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.txtOutAvgMean);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.txtOutAvgSigma);
            this.groupBox9.Location = new System.Drawing.Point(304, 32);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(143, 73);
            this.groupBox9.TabIndex = 6;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Среднее";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 48);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "M:";
            // 
            // txtOutAvgMean
            // 
            this.txtOutAvgMean.Location = new System.Drawing.Point(37, 45);
            this.txtOutAvgMean.Name = "txtOutAvgMean";
            this.txtOutAvgMean.ReadOnly = true;
            this.txtOutAvgMean.Size = new System.Drawing.Size(100, 20);
            this.txtOutAvgMean.TabIndex = 3;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "σ:";
            // 
            // txtOutAvgSigma
            // 
            this.txtOutAvgSigma.Location = new System.Drawing.Point(37, 19);
            this.txtOutAvgSigma.Name = "txtOutAvgSigma";
            this.txtOutAvgSigma.Size = new System.Drawing.Size(100, 20);
            this.txtOutAvgSigma.TabIndex = 0;
            this.txtOutAvgSigma.Text = "7,5";
            this.txtOutAvgSigma.Enter += new System.EventHandler(this.SaveText);
            this.txtOutAvgSigma.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.txtOutMaxMean);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtOutMaxSigma);
            this.groupBox3.Location = new System.Drawing.Point(155, 32);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(143, 73);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Максимальное";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "M:";
            // 
            // txtOutMaxMean
            // 
            this.txtOutMaxMean.Location = new System.Drawing.Point(37, 45);
            this.txtOutMaxMean.Name = "txtOutMaxMean";
            this.txtOutMaxMean.ReadOnly = true;
            this.txtOutMaxMean.Size = new System.Drawing.Size(100, 20);
            this.txtOutMaxMean.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "σ:";
            // 
            // txtOutMaxSigma
            // 
            this.txtOutMaxSigma.Location = new System.Drawing.Point(37, 19);
            this.txtOutMaxSigma.Name = "txtOutMaxSigma";
            this.txtOutMaxSigma.Size = new System.Drawing.Size(100, 20);
            this.txtOutMaxSigma.TabIndex = 0;
            this.txtOutMaxSigma.Text = "10";
            this.txtOutMaxSigma.Enter += new System.EventHandler(this.SaveText);
            this.txtOutMaxSigma.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.txtOutMinMean);
            this.groupBox6.Controls.Add(this.txtOutMinSigma);
            this.groupBox6.Location = new System.Drawing.Point(6, 32);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(143, 73);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Минимальное";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "M:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "σ:";
            // 
            // txtOutMinMean
            // 
            this.txtOutMinMean.Location = new System.Drawing.Point(37, 45);
            this.txtOutMinMean.Name = "txtOutMinMean";
            this.txtOutMinMean.ReadOnly = true;
            this.txtOutMinMean.Size = new System.Drawing.Size(100, 20);
            this.txtOutMinMean.TabIndex = 5;
            // 
            // txtOutMinSigma
            // 
            this.txtOutMinSigma.Location = new System.Drawing.Point(37, 19);
            this.txtOutMinSigma.Name = "txtOutMinSigma";
            this.txtOutMinSigma.Size = new System.Drawing.Size(100, 20);
            this.txtOutMinSigma.TabIndex = 0;
            this.txtOutMinSigma.Text = "5";
            this.txtOutMinSigma.Enter += new System.EventHandler(this.SaveText);
            this.txtOutMinSigma.Leave += new System.EventHandler(this.PositiveDoubleCheckFix);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(189, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Закон Рэлея";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rbPerTransactTime);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this.rbWholeTime);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.txtCount);
            this.groupBox7.Location = new System.Drawing.Point(12, 276);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(689, 50);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Общие параметры";
            // 
            // rbPerTransactTime
            // 
            this.rbPerTransactTime.AutoSize = true;
            this.rbPerTransactTime.Location = new System.Drawing.Point(230, 20);
            this.rbPerTransactTime.Name = "rbPerTransactTime";
            this.rbPerTransactTime.Size = new System.Drawing.Size(139, 17);
            this.rbPerTransactTime.TabIndex = 1;
            this.rbPerTransactTime.Text = "Для одного транзакта";
            this.rbPerTransactTime.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(98, 13);
            this.label30.TabIndex = 5;
            this.label30.Text = "Рассчет времени:";
            // 
            // rbWholeTime
            // 
            this.rbWholeTime.AutoSize = true;
            this.rbWholeTime.Checked = true;
            this.rbWholeTime.Location = new System.Drawing.Point(110, 20);
            this.rbWholeTime.Name = "rbWholeTime";
            this.rbWholeTime.Size = new System.Drawing.Size(114, 17);
            this.rbWholeTime.TabIndex = 0;
            this.rbWholeTime.TabStop = true;
            this.rbWholeTime.Text = "Для всей модели";
            this.rbWholeTime.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(447, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Количество транзактов:";
            // 
            // txtCount
            // 
            this.txtCount.Location = new System.Drawing.Point(583, 19);
            this.txtCount.Name = "txtCount";
            this.txtCount.Size = new System.Drawing.Size(100, 20);
            this.txtCount.TabIndex = 3;
            this.txtCount.Text = "500";
            this.txtCount.Enter += new System.EventHandler(this.SaveText);
            this.txtCount.Leave += new System.EventHandler(this.PositiveIntCheckFix);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox12);
            this.groupBox8.Controls.Add(this.groupBox11);
            this.groupBox8.Location = new System.Drawing.Point(474, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(403, 258);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Результаты";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.groupBox13);
            this.groupBox12.Controls.Add(this.label28);
            this.groupBox12.Controls.Add(this.txtTimeHappened);
            this.groupBox12.Location = new System.Drawing.Point(10, 154);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(387, 97);
            this.groupBox12.TabIndex = 5;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Время";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label29);
            this.groupBox13.Controls.Add(this.label27);
            this.groupBox13.Controls.Add(this.txtTimeExpected1);
            this.groupBox13.Controls.Add(this.txtTimeExpected2);
            this.groupBox13.Location = new System.Drawing.Point(193, 16);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(188, 73);
            this.groupBox13.TabIndex = 10;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Ожидаемое";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(9, 48);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 13);
            this.label29.TabIndex = 11;
            this.label29.Text = "Формула 2:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 13);
            this.label27.TabIndex = 10;
            this.label27.Text = "Формула 1:";
            // 
            // txtTimeExpected1
            // 
            this.txtTimeExpected1.Location = new System.Drawing.Point(82, 19);
            this.txtTimeExpected1.Name = "txtTimeExpected1";
            this.txtTimeExpected1.ReadOnly = true;
            this.txtTimeExpected1.Size = new System.Drawing.Size(100, 20);
            this.txtTimeExpected1.TabIndex = 9;
            // 
            // txtTimeExpected2
            // 
            this.txtTimeExpected2.Location = new System.Drawing.Point(82, 45);
            this.txtTimeExpected2.Name = "txtTimeExpected2";
            this.txtTimeExpected2.ReadOnly = true;
            this.txtTimeExpected2.Size = new System.Drawing.Size(100, 20);
            this.txtTimeExpected2.TabIndex = 5;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(8, 38);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(70, 13);
            this.label28.TabIndex = 8;
            this.label28.Text = "Полученное:";
            // 
            // txtTimeHappened
            // 
            this.txtTimeHappened.Location = new System.Drawing.Point(83, 35);
            this.txtTimeHappened.Name = "txtTimeHappened";
            this.txtTimeHappened.ReadOnly = true;
            this.txtTimeHappened.Size = new System.Drawing.Size(100, 20);
            this.txtTimeHappened.TabIndex = 7;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtB3);
            this.groupBox11.Controls.Add(this.label8);
            this.groupBox11.Controls.Add(this.label18);
            this.groupBox11.Controls.Add(this.txtYMinMin);
            this.groupBox11.Controls.Add(this.txtB2);
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Controls.Add(this.label19);
            this.groupBox11.Controls.Add(this.txtYMaxMin);
            this.groupBox11.Controls.Add(this.txtB1);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this.label20);
            this.groupBox11.Controls.Add(this.txtYMinMax);
            this.groupBox11.Controls.Add(this.txtB0);
            this.groupBox11.Controls.Add(this.label13);
            this.groupBox11.Controls.Add(this.label21);
            this.groupBox11.Controls.Add(this.txtYMaxMax);
            this.groupBox11.Location = new System.Drawing.Point(10, 19);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(387, 129);
            this.groupBox11.TabIndex = 5;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Коэффициенты";
            // 
            // txtB3
            // 
            this.txtB3.Location = new System.Drawing.Point(275, 97);
            this.txtB3.Name = "txtB3";
            this.txtB3.ReadOnly = true;
            this.txtB3.Size = new System.Drawing.Size(100, 20);
            this.txtB3.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "y1 (Min/Min):";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(247, 100);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "b3:";
            // 
            // txtYMinMin
            // 
            this.txtYMinMin.Location = new System.Drawing.Point(83, 19);
            this.txtYMinMin.Name = "txtYMinMin";
            this.txtYMinMin.ReadOnly = true;
            this.txtYMinMin.Size = new System.Drawing.Size(100, 20);
            this.txtYMinMin.TabIndex = 1;
            // 
            // txtB2
            // 
            this.txtB2.Location = new System.Drawing.Point(275, 71);
            this.txtB2.Name = "txtB2";
            this.txtB2.ReadOnly = true;
            this.txtB2.Size = new System.Drawing.Size(100, 20);
            this.txtB2.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "y2 (Max/Min):";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(247, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "b2:";
            // 
            // txtYMaxMin
            // 
            this.txtYMaxMin.Location = new System.Drawing.Point(83, 45);
            this.txtYMaxMin.Name = "txtYMaxMin";
            this.txtYMaxMin.ReadOnly = true;
            this.txtYMaxMin.Size = new System.Drawing.Size(100, 20);
            this.txtYMaxMin.TabIndex = 3;
            // 
            // txtB1
            // 
            this.txtB1.Location = new System.Drawing.Point(275, 45);
            this.txtB1.Name = "txtB1";
            this.txtB1.ReadOnly = true;
            this.txtB1.Size = new System.Drawing.Size(100, 20);
            this.txtB1.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "y3 (Min/Max):";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(247, 48);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 13);
            this.label20.TabIndex = 10;
            this.label20.Text = "b1:";
            // 
            // txtYMinMax
            // 
            this.txtYMinMax.Location = new System.Drawing.Point(83, 71);
            this.txtYMinMax.Name = "txtYMinMax";
            this.txtYMinMax.ReadOnly = true;
            this.txtYMinMax.Size = new System.Drawing.Size(100, 20);
            this.txtYMinMax.TabIndex = 5;
            // 
            // txtB0
            // 
            this.txtB0.Location = new System.Drawing.Point(275, 19);
            this.txtB0.Name = "txtB0";
            this.txtB0.ReadOnly = true;
            this.txtB0.Size = new System.Drawing.Size(100, 20);
            this.txtB0.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "y4 (Max/Max):";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(247, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(22, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "b0:";
            // 
            // txtYMaxMax
            // 
            this.txtYMaxMax.Location = new System.Drawing.Point(83, 97);
            this.txtYMaxMax.Name = "txtYMaxMax";
            this.txtYMaxMax.ReadOnly = true;
            this.txtYMaxMax.Size = new System.Drawing.Size(100, 20);
            this.txtYMaxMax.TabIndex = 7;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(707, 276);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(169, 50);
            this.btnCalculate.TabIndex = 4;
            this.btnCalculate.Text = "Рассчитать";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 333);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "Лабораторная работа 1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtInMinB;
        private System.Windows.Forms.TextBox txtInMinA;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtInMaxB;
        private System.Windows.Forms.TextBox txtInMaxA;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtOutMaxSigma;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOutMinSigma;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtInMaxMean;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtInMinMean;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtOutMaxMean;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtOutMinMean;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCount;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtYMaxMax;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtYMinMax;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtYMaxMin;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtYMinMin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.TextBox txtB3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtB2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtB1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtB0;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtInAvgMean;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtInAvgB;
        private System.Windows.Forms.TextBox txtInAvgA;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtOutAvgMean;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtOutAvgSigma;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtTimeExpected2;
        private System.Windows.Forms.TextBox txtTimeHappened;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtTimeExpected1;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.RadioButton rbPerTransactTime;
        private System.Windows.Forms.RadioButton rbWholeTime;
        private System.Windows.Forms.Label label30;
    }
}

