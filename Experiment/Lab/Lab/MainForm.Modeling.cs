﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab.Distributions;
using Lab.UI;

namespace Lab
{
    partial class MainForm
    {
        private RayleighDistribution SetupRaylegh(TextBox txtSigma, TextBox txtMean)
        {
            var sigma = double.Parse(txtSigma.Text);
            var distribution = new RayleighDistribution(sigma);
            txtMean.Text = distribution.Mean().ToString();
            return distribution;
        }

        private UniformDistribution SetupUniform(TextBox txtFrom, TextBox txtTo, TextBox txtMean)
        {
            var a = double.Parse(txtFrom.Text);
            var b = double.Parse(txtTo.Text);
            var distribution = new UniformDistribution(a, b);
            txtMean.Text = distribution.Mean().ToString();
            return distribution;
        }

        private void Calculate()
        {
            double[] y = CalculateYi();
            double[] b = CalculateBi(y);
            CalculateTimes(b);
        }

        private void CalculateTimes(double[] b)
        {
            double inputMean = NormalizeMean(uniformAvgGroup, uniformMinGroup, uniformMaxGroup);
            double outputMean = NormalizeMean(rayleighAvgGroup, rayleighMinGroup, rayleighMaxGroup);
            double exTime1 = b[0] + b[1] * inputMean + b[2] * outputMean;
            double exTime2 = exTime1 + b[3] * inputMean * outputMean;
            txtTimeExpected1.Text = exTime1.ToString();
            txtTimeExpected2.Text = exTime2.ToString();

            double realTime = ModelTime(uniformAvgGroup.Distribution, rayleighAvgGroup.Distribution);
            txtTimeHappened.Text = realTime.ToString();
        }

        private double[] CalculateBi(double[] y)
        {
            var b = new double[4];
            b[0] = (y[0] + y[1] + y[2] + y[3]) / 4.0;
            b[1] = (-y[0] + y[1] - y[2] + y[3]) / 4.0;
            b[2] = (-y[0] - y[1] + y[2] + y[3]) / 4.0;
            b[3] = (y[0] - y[1] - y[2] + y[3]) / 4.0;

            txtB0.Text = b[0].ToString();
            txtB1.Text = b[1].ToString();
            txtB2.Text = b[2].ToString();
            txtB3.Text = b[3].ToString();

            return b;
        }

        private double[] CalculateYi()
        {
            var y = new double[4];
            y[0] = ModelTime(uniformMinGroup.Distribution, rayleighMinGroup.Distribution);
            y[1] = ModelTime(uniformMaxGroup.Distribution, rayleighMinGroup.Distribution);
            y[2] = ModelTime(uniformMinGroup.Distribution, rayleighMaxGroup.Distribution);
            y[3] = ModelTime(uniformMaxGroup.Distribution, rayleighMaxGroup.Distribution);

            txtYMinMin.Text = y[0].ToString();
            txtYMaxMin.Text = y[1].ToString();
            txtYMinMax.Text = y[2].ToString();
            txtYMaxMax.Text = y[3].ToString();

            return y;
        }

        private double ModelTime(Distribution input, Distribution output)
        {
            var model = new Model(input, output);
            SimulationStats stats = model.Simulate(count);
            if (CalculateWholeTimeMode())
                return stats.TimeSpent;
            else
                return stats.TimeSpentPerTransactTerminated;
        }

        private void ErrorMessage(string message)
        {
            MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private double NormalizeMean(ParametersGroup avgGroup, ParametersGroup minGroup, ParametersGroup maxGroup)
        {
            double avg = avgGroup.GetMeanValue();
            double min = minGroup.GetMeanValue();
            double max = maxGroup.GetMeanValue();

            if (max == min)
                return 0.0;

            return (avg - min) / (max - min) * 2.0 - 1.0;
        }
    }
}
