﻿using Lab.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab
{
    class Model
    {
        public Model(Distribution input, Distribution output)
        {
            Input = input;
            Output = output;
        }

        public Distribution Input
        {
            get => input;
            set
            {
                input = value ?? throw new ArgumentNullException();
            }
        }

        public Distribution Output
        {
            get => output;
            set
            {
                output = value ?? throw new ArgumentNullException();
            }
        }

        private Distribution input;
        private Distribution output;

        public SimulationStats Simulate(int transactsCount)
        {
            var stats = new SimulationStats();

            double inputTime = input.Generate();
            double outputTime = 0.0;
            stats.TimeSpent = 0.0;
            stats.TransactsGenerated = 1;
            stats.TransactsTerminated = 0;

            while (stats.TransactsTerminated < transactsCount)
            {
                if (stats.TransactsGenerated > stats.TransactsTerminated)
                {
                    stats.TransactsTerminated++;
                    outputTime = output.Generate();
                    stats.TimeSpent += outputTime;
                }
                
                if (stats.TransactsGenerated == stats.TransactsTerminated)
                {
                    stats.TimeSpent += inputTime;
                    inputTime = 0.0;
                }

                while (outputTime > inputTime)
                {
                    stats.TransactsGenerated++;
                    inputTime += input.Generate();
                }
                inputTime -= outputTime;

                if (stats.TransactsTerminated == transactsCount)
                    stats.TransactsGenerated--;
            }

            return stats;
        }
    }
}
