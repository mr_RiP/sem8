﻿using Lab.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab
{
    partial class MainForm
    {
        private UniformParametersGroup uniformMinGroup;
        private UniformParametersGroup uniformMaxGroup;
        private UniformParametersGroup uniformAvgGroup;
        private UniformParametersGroup[] uniformGroups;

        private RayleighParametersGroup rayleighMinGroup;
        private RayleighParametersGroup rayleighMaxGroup;
        private RayleighParametersGroup rayleighAvgGroup;
        private RayleighParametersGroup[] rayleighGroups;

        private int count;

        private void InitializeParameterGroups()
        {
            uniformMinGroup = new UniformParametersGroup(txtInMinA, txtInMinB, txtInMinMean, SetupUniform(txtInMinA, txtInMinB, txtInMinMean));
            uniformMaxGroup = new UniformParametersGroup(txtInMaxA, txtInMaxB, txtInMaxMean, SetupUniform(txtInMaxA, txtInMaxB, txtInMaxMean));
            uniformAvgGroup = new UniformParametersGroup(txtInAvgA, txtInAvgB, txtInAvgMean, SetupUniform(txtInAvgA, txtInAvgB, txtInAvgMean));
            uniformGroups = new UniformParametersGroup[] { uniformMinGroup, uniformMaxGroup, uniformAvgGroup };

            rayleighMinGroup = new RayleighParametersGroup(txtOutMinSigma, txtOutMinMean, SetupRaylegh(txtOutMinSigma, txtOutMinMean));
            rayleighMaxGroup = new RayleighParametersGroup(txtOutMaxSigma, txtOutMaxMean, SetupRaylegh(txtOutMaxSigma, txtOutMaxMean));
            rayleighAvgGroup = new RayleighParametersGroup(txtOutAvgSigma, txtOutAvgMean, SetupRaylegh(txtOutAvgSigma, txtOutAvgMean));
            rayleighGroups = new RayleighParametersGroup[] { rayleighMinGroup, rayleighMaxGroup, rayleighAvgGroup };

            count = int.Parse(txtCount.Text);
        }

        private bool UniformParameter(TextBox textBox)
        {
            return uniformGroups.Any(g => g.Contains(textBox));
        }

        private void UniformCheckFix(TextBox textBox)
        {
            UniformParametersGroup group = uniformGroups.Single(g => g.Contains(textBox));
            if (double.Parse(group.A.Text) > double.Parse(group.B.Text))
            {
                ErrorMessage("Значение A должно быть меньше или равно значению B");
                ResetText(textBox);
            }
            else
                group.UpdateData();
        }

        private void RayleighCheckFix(TextBox textBox)
        {
            RayleighParametersGroup group = rayleighGroups.Single(g => g.Contains(textBox));
            group.UpdateData();
        }
    }
}
