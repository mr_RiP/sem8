﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab
{
    class SimulationStats
    {
        public double TimeSpent { get; set; }

        public int TransactsGenerated { get; set; }

        public int TransactsTerminated { get; set; }

        public double TimeSpentPerTransactTerminated
        {
            get
            {
                return TimeSpent / TransactsTerminated;
            }
        }

        public double TimeSpentPerTransactGenerated
        {
            get
            {
                return TimeSpent / TransactsGenerated;
            }
        }
    }
}
