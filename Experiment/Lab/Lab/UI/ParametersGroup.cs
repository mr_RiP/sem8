﻿using Lab.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.UI
{
    abstract class ParametersGroup
    {
        public double GetMeanValue()
        {
            return GetDistribution().Mean();
        }

        public abstract Distribution GetDistribution();
    }
}
