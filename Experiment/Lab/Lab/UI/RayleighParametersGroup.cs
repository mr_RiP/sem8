﻿using Lab.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab.UI
{
    class RayleighParametersGroup : ParametersGroup
    {
        public RayleighParametersGroup(TextBox sigma, TextBox mean, RayleighDistribution distribution)
        {
            Sigma = sigma;
            Mean = mean;
            Distribution = distribution;
        }

        public TextBox Sigma { get; set; }

        public TextBox Mean { get; set; }

        public RayleighDistribution Distribution { get; set; }

        public bool Contains(TextBox textBox)
        {
            return textBox == Sigma || textBox == Mean;
        }

        public override Distribution GetDistribution()
        {
            return Distribution;
        }

        public void UpdateData()
        {
            Distribution.Sigma = double.Parse(Sigma.Text);
            Mean.Text = Distribution.Mean().ToString();
        }
    }
}
