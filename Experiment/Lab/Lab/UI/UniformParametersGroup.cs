﻿using Lab.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab.UI
{
    class UniformParametersGroup : ParametersGroup
    {
        public UniformParametersGroup(TextBox a, TextBox b, TextBox mean, UniformDistribution distribution)
        {
            A = a;
            B = b;
            Mean = mean;
            Distribution = distribution;
        }

        public TextBox A { get; set; }
        public TextBox B { get; set; }
        public TextBox Mean { get; set; }
        public UniformDistribution Distribution { get; set; }

        public bool Contains(TextBox textBox)
        {
            return textBox == A || textBox == B || textBox == Mean;
        }

        public override Distribution GetDistribution()
        {
            return Distribution;
        }

        public void UpdateData()
        {
            Distribution.A = double.Parse(A.Text);
            Distribution.B = double.Parse(B.Text);
            Mean.Text = Distribution.Mean().ToString();
        }
    }
}
