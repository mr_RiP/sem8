﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Distributions
{
    class UniformDistribution : Distribution
    {
        public UniformDistribution(double a, double b)
        {
            if (a > b)
                throw new ArgumentOutOfRangeException();

            this.a = a;
            this.b = b;
        }

        public double A
        {
            get => a;
            set
            {
                a = value <= b ? value : throw new ArgumentOutOfRangeException();
            }
        }

        public double B
        {
            get => b;
            set
            {
                b = value >= a ? value : throw new ArgumentOutOfRangeException();
            }
        }

        private double a;

        private double b;

        public override double Mean()
        {
            return (a + b) / 2.0;
        }

        public override double Generate()
        {
            return StandardUniform() * (b - a) + a;
        }

        public override double Variance()
        {
            double diff = b - a;
            return diff * diff / 12.0;
        }
    }
}
