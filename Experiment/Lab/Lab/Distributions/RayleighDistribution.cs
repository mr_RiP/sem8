﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Distributions
{
    class RayleighDistribution : Distribution
    {
        public RayleighDistribution(double sigma)
        {
            Sigma = sigma;
        }

        public double Sigma
        {
            get => sigma;
            set
            {
                sigma = value > 0 ? value : throw new ArgumentOutOfRangeException();
            }
        }

        private double sigma;

        public override double Generate()
        {
            return sigma * Math.Sqrt(-2.0 * Math.Log(StandardUniform()));
        }

        public override double Mean()
        {
            return sigma * Math.Sqrt(Math.PI / 2.0);
        }

        public override double Variance()
        {
            return sigma * sigma * (4.0 - Math.PI) / 2.0;
        }
    }
}
