﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Distributions
{
    abstract class Distribution
    {
        public abstract double Mean();

        public abstract double Variance();

        public abstract double Generate();

        private static Random random = new Random();

        private static object lockObject = new object();

        protected static double StandardUniform()
        {
            lock (lockObject)
                return random.NextDouble() + double.Epsilon;
        }
    }
}
